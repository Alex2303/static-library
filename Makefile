CC =	gcc
NAME =	libmy.a
SRC =	./lib/my/my_getnbr.c		\
		./lib/my/my_putstr.c		\
		./lib/my/my_strdup.c		\
		./lib/my/my_strncpy.c    \
		./lib/my/my_isneg.c		\
		./lib/my/my_strcat.c		\
		./lib/my/my_strlen.c		\
		./lib/my/my_strstr.c		\
		./lib/my/my_putchar.c	\
		./lib/my/my_strcmp.c		\
		./lib/my/my_strncat.c	\
		./lib/my/my_swap.c		\
		./lib/my/my_putnbr.c		\
		./lib/my/my_strcpy.c		\
		./lib/my/my_strncmp.c		\
		./lib/my/my_str_to_word_array.c		\
		./lib/my/my_readline.c		
OBJ	=	$(SRC:%.c=%.o)
RM	=	rm -f
LIB =	ar r $(NAME) $(OBJ) 
CP 	=	cp $(NAME) ./lib \

$(NAME):	$(OBJ)
			$(LIB)

all:	$(NAME)

clean:
		$(RM) $(OBJ)
fclean:		clean
		$(RM) $(NAME)
re:		fclean all
