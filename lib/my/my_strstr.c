/*
** ETNA PROJECT, 02/11/2019 by desous_a
** my_strstr
** File description:
** function finds the first occurrence of the substring needle in the string haystack. The terminating '\0' characters are not compared.
*/
#include <stddef.h>

int my_strlen(const char *str);

int my_strncmp(const char *s1, const char *s2, int n);

char *my_strstr(char *str, const char *to_find)
{
    int i;
    int j;

    i = 0;
    j = 0;

    while ((str[i] != '\0') && (to_find[j] != '\0')) {
        if (my_strncmp(str, to_find, my_strlen(to_find)) == 0) {
            return (str);
        }
        str++;
    }
    return NULL;
}
