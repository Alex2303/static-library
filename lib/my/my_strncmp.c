/*
** ETNA PROJECT, 25/10/2019 by desous_a
** my_strncmp
** File description:
**      my_strncmp
*/
int my_strncmp(const char *s1, const char *s2, int n)
{
    int i;

    i = 0;

    while ((s1[i] != '\0') && (i < n) && (s1[i] == s2[i])) {
        i++;
    }
    return s1[i] - s2[i]; 
}
