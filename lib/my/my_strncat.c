/*
** ETNA PROJECT, 02/11/2019 by desous_a
** my_strcat
** File description:
** The function my_strncat appends the string pointed to by src to the end of the string pointed to by dest. 
*/

int my_strlen(const char *str);

char *my_strncat(char *dest, const char *src, int nb)
{
    int i;
    int j;

    i = my_strlen(dest);
    j = 0;

    while ((src[j] != '\0') && (j < nb)) {
        dest[i] = src[j];

        i++;
        j++;
    }
    dest[i] = '\0';
    return dest;
}

