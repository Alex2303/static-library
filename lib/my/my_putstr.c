/*
** ETNA PROJECT, 22/10/2019 by desous_a
** my_putstr
** File description:
** writes a string to stdout up to but not including the null character. A newline character is appended to the output.
*/
void my_putchar(char c);

void my_putstr(const char *str)
{
    int i = 0;
    
    while (str[i] != '\0') {
    my_putchar(str[i]);
    i = i + 1;
    }
}


