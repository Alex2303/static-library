/*
** ETNA PROJECT, 25/10/2019 by desous_a
** my_putnbr
** File description:
**      my-putnbr
*/

#include <unistd.h>

void my_putchar(char c)
{
    write(1, &c, 1);
}

int my_putnbr(int nb)
{
    if (nb < 0) {
        my_putchar('-');
        nb = nb *(-1);
    }
    if (nb >= 0 && nb <= 9) {
        my_putchar(nb  + '0');
    }
    if(nb > 9) {
        my_putnbr(nb/10);
        my_putnbr(nb%10);
    }
}

