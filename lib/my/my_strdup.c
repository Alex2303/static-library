/*
** ETNA PROJECT, 23/10/2019 by desous_a
** my_strdup
** File description:
** The function strdup() is used to duplicate a string. It returns a pointer to null-terminated byte string.
*/

#include <stdlib.h>

void my_putchar(char c);

int my_strlen(const char *str);

char *my_strcpy(char *dest, const char *src);

char *my_strdup(const char *src)
{
    char *ptr;
   
    ptr = malloc(my_strlen(src) * sizeof(char));
    ptr = my_strcpy(ptr, src);
    return ptr;
}

