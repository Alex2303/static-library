/*
** ETNA PROJECT, 22/10/2019 by desous_a
** my_strlen
** File description:
** The strlen() function calculates the length of a given string.
*/
int my_strlen(const char *str)
{
    int i = 0;
    
    while (str[i] != '\0')
        {
            i++;
        }
    return i;
}

 
