/*
** ETNA PROJECT, 23/10/2019 by desous_a
** my_strcpy
** File description:
** Write a function that copies a string into another. The destination string will already have enough memory to copy the source string.
*/
char *my_strcpy(char *dest, const char *src)
{
    int i = 0;
    
    while (src[i] != '\0')
        {
            dest[i] = src[i]; 
            i++;
        }
    return(dest);
}

