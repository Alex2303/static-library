/*
** ETNA PROJECT, 28/10/2019 by desous_a
** my_str_word_array
** File description:
** Write a function that splits a string into words. Separators will all be non-alphanumeric characters.The function returns an array in which each cell contains the address of a string (representing a word).The last cell must be null to terminate the array.*/

#include <stdlib.h>
#include <stdio.h>

char *my_strncpy(char *dest, const char *src, int n)
{
    int i = 0;

    while (i < n) {
        dest[i] = src[i];
        i++;
    }
    dest[i] = '\0';
    return dest;
}

int is_alpha_numeric(char c)
{
    if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9')))
        return 0;
    return 1;
}

int countWords(const char *str)
{
    int wordCount = 0;
    int i = 0;
    
    while (str[i] != '\0') {
        while ((str[i] != '\0') && (is_alpha_numeric(str[i]) == 1)) {
            i++;
        }    
        while (str[i] != '\0' && is_alpha_numeric(str[i]) == 0) {
            i++;
        }
        wordCount++;
    }
    return wordCount;
}           
             
char **my_str_to_word_array(const char *str)
{
    char **tab;
    int start = 0;
    int end = 0;
    int i = 0;
    int k = 0;
    
    tab = malloc(sizeof(char *) * (countWords(str) + 1));
    
    while (str[start] != '\0') {
        while ((is_alpha_numeric(str[start]) == 1) && str[start] != '\0') {
            start++;
        }
        end = start;
        while ((is_alpha_numeric(str[end]) == 0) && str[end] != '\0') {
            end++;
        } 
        tab[k] = malloc(sizeof(char) * (end - start) + 1);
        while (start < end) {
            tab[k][i] = str[start];
            start++;
            i++;
        }
        tab[k][i] = '\0';
        k++;
        i = 0;
    }
    tab[k] = NULL;
    return tab;
}

int main(int argc, char **argv)
{
    char *str = "bonjour les    amis";
    char **var = my_str_to_word_array(str);
    for (int i = 0; var[i] != NULL; i++)
        printf("tab[%d] = %s\n", i, var[i]);
    return 0;
}
