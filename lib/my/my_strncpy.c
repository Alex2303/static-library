/*
** ETNA PROJECT, 23/10/2019 by desous_a
** my_strncpy
** File description:
** copies up to n characters from the string pointed to, by src to dest. In a case where the length of src is less than that of n, the remainder of dest will be padded with null bytes.
*/
char *my_strncpy(char *dest, const char *src, int n)
{
    int i = 0;
    
    while (i < n) {
            dest[i] = src[i];
            i++; 
        }
    dest[i] = '\0';
    return(dest);
}

