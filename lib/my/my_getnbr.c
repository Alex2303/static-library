/*
** ETNA PROJECT, 27/10/2019 by desous_a
** my_getnbr
** File description:
** Write a function that parses a number from a string representation taken as parameter andreturns it. */


int my_getnbr(const char *str)
{
    int i;
    long result;
    int signe;
    int MIN;
    int MAX;

    MIN = -2147483648;
    MAX = 2147483647;
    i = 0;
    result = 0;
    signe = 1;

    while ((str[i] != '\0') && (((str[i] >= '0') && (str[i] <= '9')) || (str[i] == '+') || (str[i] == '-'))) {
            if (str[i] >= '0' && str[i] <= '9') { 
                result = result * 10 + (str[i] - '0');    
            }
            if (str[i] == '+') { 
                signe = signe * 1;
            }
            if (str[i] == '-') { 
                signe = signe * (-1);
            }
            i++;
        }
    if (result <MIN && result >MAX)
        return (0);
    
        return result * signe;
}
