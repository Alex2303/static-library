/*
** ETNA PROJECT, 21/10/2019 by desous_a
** my_isneg
** File description:
**      my_isneg
*/
void my_putchar(char c);

void my_isneg(int n)  
{
    if (n < 0) {
        my_putchar('N');
    }
    else {
        my_putchar('P');
    }
}


