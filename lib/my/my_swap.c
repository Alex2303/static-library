/*
** ETNA PROJECT, 22/10/2019 by desous_a
** my_swap
** File description:
** The my_swap() function is used to swap two numbers.
*/
void my_swap(int *a, int *b)
{
    int t;
    
    t = *a;
    *a = *b;
    *b = t;
}


